package com.sky.vod.parentalcontrol;

import java.util.Arrays;
import java.util.Optional;

public enum ControlLevelType {

    U("U", 1), PG("PG", 2), Age12("12", 3), Age15("15", 4), Age18("18", 5);

    private String name;
    private int level;

    ControlLevelType(String name, int level) {
        this.name = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public static ControlLevelType getType(final String level) {
        Optional<ControlLevelType> type = Arrays.stream(ControlLevelType.values())
                .filter(pct -> null != level && pct.getName().equals(level))
                .findFirst();
        return type.isPresent() ? type.get() : null;
    }
}
