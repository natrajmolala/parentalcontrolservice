package com.sky.vod.parentalcontrol;

import com.sky.vod.moviemetadata.MovieService;
import com.sky.vod.moviemetadata.TechnicalFailureException;
import com.sky.vod.moviemetadata.TitleNotFoundException;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ParentalControlService {

    private static final Logger LOGGER = Logger.getLogger(ParentalControlService.class.getName());

    private MovieService movieService;

    public boolean isAccessible(ControlLevelType preferredControlLevel, String movieId) throws TitleNotFoundException {

        if (null == preferredControlLevel || null == movieId) {
            return false;
        }

        boolean accessible = false;
        try {
            String movieControlLevel = movieService.getParentalControlLevel(movieId);
            ControlLevelType movieControlLevelType = ControlLevelType.getType(movieControlLevel);

            accessible = movieControlLevelType != null && movieControlLevelType.getLevel() <= preferredControlLevel.getLevel();

        } catch (TechnicalFailureException tfe) {
            LOGGER.log(Level.WARNING, "Error getting parental control level for movieId: " + movieId, tfe);
        }

        return accessible;
    }
}
