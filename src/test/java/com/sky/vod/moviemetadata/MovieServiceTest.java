package com.sky.vod.moviemetadata;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

public class MovieServiceTest {

    @Mock
    private MovieService movieService;

    @Before
    public void beforeTest() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = TitleNotFoundException.class)
    public void shouldThrowTitleNotFoundExceptionWhenNoMovieFound() throws Exception {
        String movieId = "movieIdNotFound";
        given(movieService.getParentalControlLevel(movieId)).willThrow(TitleNotFoundException.class);

        movieService.getParentalControlLevel(movieId);

        fail("No TitleNotFoundException");
    }

    @Test(expected = TechnicalFailureException.class)
    public void shouldThrowTechnicalFailureExceptionOnCallingMovieService() throws Exception {
        String movieId = "movieIdTechFail";
        given(movieService.getParentalControlLevel(movieId)).willThrow(TechnicalFailureException.class);

        movieService.getParentalControlLevel(movieId);

        fail("No TechnicalFailureException");
    }

    @Test
    public void shouldReturnValidParentalControlLevelForTheMovieId() throws Exception {
        String movieId = "movieIdValid";
        when(movieService.getParentalControlLevel(movieId)).thenReturn("PG");

        String parentalControlLevel = movieService.getParentalControlLevel(movieId);

        assertThat(parentalControlLevel).isEqualTo("PG");
    }
}
