package com.sky.vod.parentalcontrol;

import com.sky.vod.moviemetadata.MovieService;
import com.sky.vod.moviemetadata.TechnicalFailureException;
import com.sky.vod.moviemetadata.TitleNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class ParentalControlServiceTest {

    @InjectMocks
    private ParentalControlService parentalControlService;

    @Mock
    private MovieService movieService;

    public void shouldReturnFalseWhenControlLevelParamIsNull() throws Exception {
        String movieId = "movieIdNotFound";

        boolean isAccessible = parentalControlService.isAccessible(null, movieId);

        assertThat(isAccessible).isFalse();
    }

    public void shouldReturnFalseWhenMovieIdParamIsNull() throws Exception {
        String movieId = null;

        boolean isAccessible = parentalControlService.isAccessible(ControlLevelType.PG, movieId);

        assertThat(isAccessible).isFalse();
    }

    @Test(expected = TitleNotFoundException.class)
    public void shouldThrowTitleNotFoundExceptionWhenAccessingMovieThatIsNotExisting() throws Exception {
        String movieId = "movieIdNotFound";
        given(movieService.getParentalControlLevel(movieId)).willThrow(TitleNotFoundException.class);

        parentalControlService.isAccessible(ControlLevelType.PG, movieId);

        fail("No TitleNotFoundException");
    }

    @Test
    public void shouldReturnFalseWhenThereIsATechnicalFailureCallingMovieService() throws Exception {
        String movieId = "movieIdTechFail";
        given(movieService.getParentalControlLevel(movieId)).willThrow(TechnicalFailureException.class);

        boolean isAccessible = parentalControlService.isAccessible(ControlLevelType.PG, movieId);

        assertThat(isAccessible).isFalse();
    }

    @Test
    public void shouldReturnTrueWhenPreferredControlLevelIsSameAsTheMovieControlLevel() throws Exception {
        String movieId = "movieIdControlMatch";
        given(movieService.getParentalControlLevel(movieId)).willReturn(ControlLevelType.PG.getName());

        boolean isAccessible = parentalControlService.isAccessible(ControlLevelType.PG, movieId);

        assertThat(isAccessible).isTrue();
    }

    @Test
    public void shouldReturnTrueWhenPreferredControlLevelIs_PGAndMovieControlLevel_U() throws Exception {
        String movieId = "movieIdForPG";
        given(movieService.getParentalControlLevel(movieId)).willReturn(ControlLevelType.U.getName());

        boolean isAccessible = parentalControlService.isAccessible(ControlLevelType.PG, movieId);

        assertThat(isAccessible).isTrue();
    }

    @Test
    public void shouldReturnTrueWhenPreferredParentalLevel_18AndMovieControlLevel_PG() throws Exception {
        String movieId = "movieIdFor18";
        given(movieService.getParentalControlLevel(movieId)).willReturn(ControlLevelType.PG.getName());

        boolean isAccessible = parentalControlService.isAccessible(ControlLevelType.Age18, movieId);

        assertThat(isAccessible).isTrue();
    }

    @Test
    public void shouldReturnTrueWhenPreferredParentalLevel_18AndMovieControlLevel_12() throws Exception {
        String movieId = "movieIdFor18";
        given(movieService.getParentalControlLevel(movieId)).willReturn(ControlLevelType.Age12.getName());

        boolean isAccessible = parentalControlService.isAccessible(ControlLevelType.Age18, movieId);

        assertThat(isAccessible).isTrue();
    }

    @Test
    public void shouldReturnFalseWhenPreferredParentalLevel_UAndMovieControlLevel_15() throws Exception {
        String movieId = "movieIdForU";
        given(movieService.getParentalControlLevel(movieId)).willReturn(ControlLevelType.Age15.getName());

        boolean isAccessible = parentalControlService.isAccessible(ControlLevelType.U, movieId);

        assertThat(isAccessible).isFalse();
    }

    @Test
    public void shouldReturnFalseWhenPreferredParentalLevel_15AndMovieControlLevel_18() throws Exception {
        String movieId = "movieIdFor15";
        given(movieService.getParentalControlLevel(movieId)).willReturn(ControlLevelType.Age18.getName());

        boolean isAccessible = parentalControlService.isAccessible(ControlLevelType.Age15, movieId);

        assertThat(isAccessible).isFalse();
    }

    @Test
    public void shouldReturnFalseWhenMoreThanPreferredParentalLevel_UAndMovieParentalControlLevel_12() throws Exception {
        String movieId = "movieIdForU";
        given(movieService.getParentalControlLevel(movieId)).willReturn(ControlLevelType.Age12.getName());

        boolean isAccessible = parentalControlService.isAccessible(ControlLevelType.U, movieId);

        assertThat(isAccessible).isFalse();
    }

}
